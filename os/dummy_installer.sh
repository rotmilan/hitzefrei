#!/usr/bin/env bash
# hitzefrei-installer: Trivial Hitzefrei-Installer setup and configuration
# Easiest setup and mangement of Hitzefrei-Installer on Debian Linux
# http://hitzefrei-installer.io
# Heavily adapted from the pi-hole.net project and...
# https://github.com/StarshipEngineer/OpenVPN/
# https://gist.github.com/philcryer/e949be81f94c2598fce01c9a2d687247
#
# Install with this command (from your Debian Linux):
#
# curl -L https://install.hitzefrei.org | bash
# Make sure you have `curl` installed


######## VARIABLES #########

tmpLog="/tmp/hitzefrei-installer-install.log"
instalLogLoc="/etc/hitzefrei-installer/install.log"

### PKG Vars ###
PKG_MANAGER="apt-get"
PKG_CACHE="/var/lib/apt/lists/"
UPDATE_PKG_CACHE="${PKG_MANAGER} update"
PKG_INSTALL="${PKG_MANAGER} --yes --no-install-recommends install"
PKG_COUNT="${PKG_MANAGER} -s -o Debug::NoLocking=true upgrade | grep -c ^Inst || true"
HITZEFREI_DEPS=( git tar wget grep whiptail )
###          ###

# todo create release
#hitzefrei-installerGitUrl="https://gitlab.com/rotmilan/hitzefrei.git"
#hitzefrei-installerFilesDir="/etc/.hitzefrei-installer"
#easyrsaVer="0.0.1-hitzefrei-installer1"
#easyrsaRel="https://gitlab.com/rotmilan/hitzefrei/-/artifacts/${easyrsaVer}/HitzefreiInstaller-${easyrsaVer}.tgz"

# Find the rows and columns. Will default to 80x24 if it can not be detected.
screen_size=$(stty size 2>/dev/null || echo 24 80)
rows=$(echo $screen_size | awk '{print $1}')
columns=$(echo $screen_size | awk '{print $2}')

# Divide by two so the dialogs take up half of the screen, which looks nice.
r=$(( rows / 2 ))
c=$(( columns / 2 ))
# Unless the screen is tiny
r=$(( r < 20 ? 20 : r ))
c=$(( c < 70 ? 70 : c ))

######## FIRST CHECK ########
# Must be root to install
echo ":::"
if [[ $EUID -eq 0 ]];then
    echo "::: You are root."
else
    echo "::: sudo will be used for the install."
    # Check if it is actually installed
    # If it isn't, exit because the install cannot complete
    if [[ $(dpkg-query -s sudo) ]];then
        export SUDO="sudo"
        export SUDOE="sudo -E"
    else
        echo "::: Please install sudo or run this as root."
        exit 1
    fi
fi

# if lsb_release command is on their system
if hash lsb_release 2>/dev/null; then
    PLAT=$(lsb_release -si)
    OSCN=$(lsb_release -sc) # We want this to be trusty xenial or jessie

    if [[ $PLAT == "Ubuntu" || $PLAT == "Raspbian" || $PLAT == "Debian" ]]; then
        if [[ $OSCN != "trusty" && $OSCN != "xenial" && $OSCN != "jessie" ]]; then
            echo "todo OS (line ~72)"
        fi
    else
        noOS_Support
    fi
# else get info from os-release
elif grep -q debian /etc/os-release; then
    if grep -q jessie /etc/os-release; then
        PLAT="Raspbian"
        OSCN="jessie"
    else
        PLAT="Ubuntu"
        OSCN="unknown"
        maybeOS_Support
    fi
# else we prob don't want to install
else
    noOS_Support
fi

echo "${PLAT} todo line ~92"

####### FUNCTIONS ##########
spinner()
{
    local pid=$1
    local delay=0.50
    local spinstr='/-\|'
    while [ "$(ps a | awk '{print $1}' | grep "${pid}")" ]; do
        local temp=${spinstr#?}
        printf " [%c]  " "${spinstr}"
        local spinstr=${temp}${spinstr%"$temp"}
        sleep ${delay}
        printf "\b\b\b\b\b\b"
    done
    printf "    \b\b\b\b"
}

welcomeDialogs() {
    # Display the welcome dialog
    whiptail --msgbox --backtitle "Welcome" --title "Hitzefrei automated installer" "This script will install some tools for you ..." ${r} ${c}

    # Explain the need for a static address
    whiptail --msgbox --backtitle "... todo (line~115)..." --title "... todo (line~115)..." ${r} ${c}
}

verifyFreeDiskSpace() {
    # If user installs unattended-upgrades we'd need about 60MB so will check for 75MB free
    echo "::: Verifying free disk space..."
    local required_free_kilobytes=76800
    local existing_free_kilobytes=$(df -Pk | grep -m1 '\/$' | awk '{print $4}')

    # - Unknown free disk space , not a integer
    if ! [[ "${existing_free_kilobytes}" =~ ^([0-9])+$ ]]; then
        echo "::: Unknown free disk space!"
        echo "::: We were unable to determine available free disk space on this system."
        echo "::: You may continue with the installation, however, it is not recommended."
        read -r -p "::: If you are sure you want to continue, type YES and press enter :: " response
        case $response in
            [Y][E][S])
                ;;
            *)
                echo "::: Confirmation not received, exiting..."
                exit 1
                ;;
        esac
    # - Insufficient free disk space
    elif [[ ${existing_free_kilobytes} -lt ${required_free_kilobytes} ]]; then
        echo "::: Insufficient Disk Space!"
        echo "::: Your system appears to be low on disk space. hitzefrei-installer recommends a minimum of $required_free_kilobytes KiloBytes."
        echo "::: You only have ${existing_free_kilobytes} KiloBytes free."
        echo "::: If this is a new install on a Debian Linux you may need to expand your disk."
        echo "::: Try running 'sudo raspi-config', and choose the 'expand file system option'"
        echo "::: After rebooting, run this installation again. (curl -L https://install.hitzefrei-installer.io | bash)"

        echo "Insufficient free space, exiting..."
        exit 1
    fi
}

installScripts() {
    # Install the scripts from /etc/.hitzefrei-installer to their various locations
    $SUDO echo ":::"
    $SUDO echo -n "::: Installing scripts to /opt/hitzefrei-installer..."
    if [ ! -d /opt/hitzefrei-installer ]; then
        $SUDO mkdir /opt/hitzefrei-installer
        $SUDO chown "$hitzefrei-installerUser":root /opt/hitzefrei-installer
        $SUDO chmod u+srwx /opt/hitzefrei-installer
    fi
    # todo
    
    . /etc/bash_completion.d/hitzefrei-installer
    # Copy interface setting for debug
    $SUDO cp /tmp/hitzefrei-installerINT /etc/hitzefrei-installer/hitzefrei-installerINTERFACE

    $SUDO echo " done."
}

package_check_install() {
    dpkg-query -W -f='${Status}' "${1}" 2>/dev/null | grep -c "ok installed" || ${PKG_INSTALL} "${1}"
}

update_package_cache() {
  #Running apt-get update/upgrade with minimal output can cause some issues with
  #requiring user input

  #Check to see if apt-get update has already been run today
  #it needs to have been run at least once on new installs!
  timestamp=$(stat -c %Y ${PKG_CACHE})
  timestampAsDate=$(date -d @"${timestamp}" "+%b %e")
  today=$(date "+%b %e")

  if [[ ${PLAT} == "Ubuntu" || ${PLAT} == "Debian" ]]; then
    if [[ ${OSCN} == "trusty" || ${OSCN} == "jessie" || ${OSCN} == "wheezy" ]]; then
      $SUDO apt-get -qq update & spinner $!
      echo " done!"
    fi
  fi

  if [ ! "${today}" == "${timestampAsDate}" ]; then
    #update package lists
    echo ":::"
    echo -n "::: ${PKG_MANAGER} update has not been run today. Running now..."
    $SUDO ${UPDATE_PKG_CACHE} &> /dev/null
    echo " done!"
  fi
}

notify_package_updates_available() {
  # Let user know if they have outdated packages on their system and
  # advise them to run a package update at soonest possible.
  echo ":::"
  echo -n "::: Checking ${PKG_MANAGER} for upgraded packages...."
  updatesToInstall=$(eval "${PKG_COUNT}")
  echo " done!"
  echo ":::"
  if [[ ${updatesToInstall} -eq "0" ]]; then
    echo "::: Your system is up to date! Continuing with hitzefrei-installer installation..."
  else
    echo "::: There are ${updatesToInstall} updates available for your system!"
    echo "::: We recommend you update your OS after installing hitzefrei-installer! "
    echo ":::"
  fi
}

install_dependent_packages() {
  # Install packages passed in via argument array
  # No spinner - conflicts with set -e
  declare -a argArray1=("${!1}")

  if command -v debconf-apt-progress &> /dev/null; then
    $SUDO debconf-apt-progress -- ${PKG_INSTALL} "${argArray1[@]}"
  else
    for i in "${argArray1[@]}"; do
      echo -n ":::    Checking for $i..."
      $SUDO package_check_install "${i}" &> /dev/null
      echo " installed!"
    done
  fi
}

stopServices() {
    # Stop Hitzefrei-Installer
    $SUDO echo ":::"
    $SUDO echo -n "::: Stopping Hitzefrei-Installer service..."
    if [[ $PLAT == "Ubuntu" || $PLAT == "Debian" ]]; then
        $SUDO service Hitzefrei-Installer stop || true
    else
        $SUDO systemctl stop Hitzefrei-Installer.service || true
    fi
    $SUDO echo " done."
}

checkForDependencies() {
    #Running apt-get update/upgrade with minimal output can cause some issues with
    #requiring user input (e.g password for phpmyadmin see #218)
    #We'll change the logic up here, to check to see if there are any updates available and
    # if so, advise the user to run apt-get update/upgrade at their own discretion
    #Check to see if apt-get update has already been run today
    # it needs to have been run at least once on new installs!

    timestamp=$(stat -c %Y /var/cache/apt/)
    timestampAsDate=$(date -d @"$timestamp" "+%b %e")
    today=$(date "+%b %e")

    if [[ $PLAT == "Ubuntu" || $PLAT == "Debian" ]]; then
        if [[ $OSCN == "trusty" || $OSCN == "jessie" || $OSCN == "wheezy" ]]; then
            wget -O - https://swupdate.Hitzefrei-Installer.net/repos/repo-public.gpg| $SUDO apt-key add -
            echo "deb http://swupdate.Hitzefrei-Installer.net/apt $OSCN main" | $SUDO tee /etc/apt/sources.list.d/swupdate.Hitzefrei-Installer.net.list > /dev/null
            echo -n "::: Adding Hitzefrei-Installer repo for $PLAT $OSCN ..."
            $SUDO apt-get -qq update & spinner $!
            echo " done!"
        fi
    fi

    if [ ! "$today" == "$timestampAsDate" ]; then
        #update package lists
        echo ":::"
        echo -n "::: apt-get update has not been run today. Running now..."
        $SUDO apt-get -qq update & spinner $!
        echo " done!"
    fi
    echo ":::"
    echo -n "::: Checking apt-get for upgraded packages...."
    updatesToInstall=$($SUDO apt-get -s -o Debug::NoLocking=true upgrade | grep -c ^Inst)
    echo " done!"
    echo ":::"
    if [[ $updatesToInstall -eq "0" ]]; then
        echo "::: Your pi is up to date! Continuing with hitzefrei-installer installation..."
    else
        echo "::: There are $updatesToInstall updates availible for your pi!"
        echo "::: We recommend you run 'sudo apt-get upgrade' after installing hitzefrei-installer! "
        echo ":::"
    fi
    echo ":::"
    echo "::: Checking dependencies:"

    dependencies=( git tar wget grep whiptail )
    for i in "${dependencies[@]}"; do
        echo -n ":::    Checking for $i..."
        if [ "$(dpkg-query -W -f='${Status}' "$i" 2>/dev/null | grep -c "ok installed")" -eq 0 ]; then
            echo -n " Not found! Installing...."            
            if [[ $i == "expect" ]] || [[ $i == "Hitzefrei-Installer" ]]; then
                ($SUDO apt-get --yes --quiet --no-install-recommends install "$i" > /dev/null || echo "Installation Failed!" && fixApt) & spinner $!
            else
                ($SUDO apt-get --yes --quiet install "$i" > /dev/null || echo "Installation Failed!" && fixApt) & spinner $!
            fi
            echo " done!"
        else
            echo " already installed!"
        fi
    done
}

getGitFiles() {
    # Setup git repos for base files
    echo ":::"
    echo "::: Checking for existing base files..."
    if is_repo "${1}"; then
        update_repo "${1}"
    else
        make_repo "${1}" "${2}"
    fi
}

is_repo() {
    # If the directory does not have a .git folder it is not a repo
    echo -n ":::    Checking $1 is a repo..."
    cd "${1}" &> /dev/null || return 1
    $SUDO git status &> /dev/null && echo " OK!"; return 0 || echo " not found!"; return 1
}

make_repo() {
    # Remove the non-repos interface and clone the interface
    echo -n ":::    Cloning $2 into $1..."
    $SUDO rm -rf "${1}"
    $SUDO git clone -q "${2}" "${1}" > /dev/null & spinner $!
    if [ -z "${TESTING+x}" ]; then
        :
    else
        $SUDO git -C "${1}" checkout test
    fi
    echo " done!"
}

update_repo() {
    # Pull the latest commits
    echo -n ":::     Updating repo in $1..."
    cd "${1}" || exit 1
    $SUDO git stash -q > /dev/null & spinner $!
    $SUDO git pull -q > /dev/null & spinner $!
    if [ -z "${TESTING+x}" ]; then
        :
    else
        ${SUDOE} git checkout test
    fi
    echo " done!"
}


installhitzefrei-installer() {
    stopServices    
    $SUDO mkdir -p /etc/hitzefrei-installer/
    getGitFiles ${hitzefrei-installerFilesDir} ${hitzefrei-installerGitUrl}
    installScripts            
}

displayFinalMessage() {
    # Final completion message to user
    if [[ $PLAT == "Ubuntu" || $PLAT == "Debian" ]]; then
        $SUDO service Hitzefrei-Installer start
    else
        $SUDO systemctl enable Hitzefrei-Installer.service
        $SUDO systemctl start Hitzefrei-Installer.service
    fi
}
    
######## SCRIPT ############
# Verify there is enough disk space for the install
verifyFreeDiskSpace

#checkForDependencies
update_package_cache

notify_package_updates_available

install_dependent_packages HITZEFREI_DEPS[@]

# Start the installer
welcomeDialogs

# Find interfaces and let the user choose one
chooseInterface

# Install
installhitzefrei-installer | tee ${tmpLog}

#Move the install log into /etc/hitzefrei-installer for storage
$SUDO mv ${tmpLog} ${instalLogLoc}

displayFinalMessage

echo "Install Complete..."
