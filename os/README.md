# Links:

GitLab repo:
- https://gitlab.com/rotmilan/hitzefrei

OpenAPI
- https://github.com/OpenAPITools/openapi-generator

REST-API Wordpress: 
- https://hitzefrei.org/installer/
- https://hitzefrei.org/wp-json/wp/v2/posts




# Acceptance Criteria:

1. get OS version:
```console
cat /etc/os-release

# 1. example output Debian OS:
...
ID=debian
VERSION_CODENAME=bullseye
...

# 2. example output Ubuntu OS:
...
ID=ubuntu
VERSION_CODENAME=Focal Fossa
...
```

2. regex match OS (debian or ubuntu) and VERSION_CODENAME
```console	
# 1. example: {OS:debian, VERSION_CODENAME:bullseye} 
# 2. example: {OS:ubuntu, VERSION_CODENAME:Focal fossa}
```

3. REST-GET of https://hitzefrei.org/wp-json/wp/v2/posts
```console
# 1. example: JSON Debian
#WP-Post-ID:729
#content/rendered:
'\n<figure class="wp-block-table"><table><tbody><tr><td><strong>VERSION_CODENAME:</strong></td><td><strong>wget URL:</strong></td></tr><tr><td>bullseye</td><td><a href="https://gitlab.com/rotmilan/hitzefrei/-/raw/Story-REST-installer/os/installer/Debian/debian_bullseye_installer.sh" target="_blank" rel="noreferrer noopener">debian_bullseye_installer.sh</a></td></tr><tr><td>bookworm</td><td><a href="https://gitlab.com/rotmilan/hitzefrei/-/raw/Story-REST-installer/os/installer/Debian/debian_bookworm_installer.sh" target="_blank" rel="noreferrer noopener">debian_bookworm_installer.sh</a></td></tr></tbody></table></figure>\n'

# 2. example: JSON Ubuntu
#WP-Post-ID:735
#content/rendered:
'\n<figure class="wp-block-table"><table><tbody><tr><td><strong>VERSION_CODENAME:</strong></td><td><strong>wget URL:</strong></td></tr><tr><td>Focal Fossa</td><td><a href="https://gitlab.com/rotmilan/hitzefrei/-/raw/Story-REST-installer/os/installer/Ubuntu/ubuntu_focal_fossa _installer.sh" target="_blank" rel="noreferrer noopener">ubuntu_focal_fossa _installer.sh</a></td></tr><tr><td>Jammy Jellyfish</td><td><a href="https://gitlab.com/rotmilan/hitzefrei/-/raw/Story-REST-installer/os/installer/Ubuntu/ubuntu_jammy_jellyfish_installer.sh" target="_blank" rel="noreferrer noopener">ubuntu_jammy_jellyfish _installer.sh</a></td></tr></tbody></table></figure>\n'
```

4. Regex JSON, match wget-URL and VERSION_CODENAME
```console
# 1. example: {OS:debian, VERSION_CODENAME:bullseye}
wget_url="https://gitlab.com/rotmilan/hitzefrei/-/raw/Story-REST-installer/os/installer/Debian/debian_bullseye_installer.sh"

# 2. example: {OS:ubuntu, VERSION_CODENAME:Focal fossa}
wget_url="https://gitlab.com/rotmilan/hitzefrei/-/raw/Story-REST-installer/os/installer/Ubuntu/ubuntu_focal_fossa%20_installer.sh"
``` 
 
5. Get the right script and run installer:
```console
wget $wget_url$
chmod +x debian_installer.sh  
./debian_installer.sh
``` 

4. Test result:
```console
Start to install some tools on Debian bullseye ...
done ...
```
