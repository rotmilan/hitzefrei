Bernd, ein erfahrener Busfahrer und Fachkraft im Fahrdienst, ist 53 Jahre alt, in einer glücklichen Ehe und hat drei Kinder sowie fünf Enkelkinder. Seine Interessen reichen von Wanderungen in den Bergen bis zu entspannten Urlauben am Gardasee.

## Beruflicher Hintergrund

Bernd ist nicht nur ein Busfahrer, sondern auch ein Fachmann im Fahrdienst. Mit einem Jahresbruttogehalt von 36.000€ trägt er täglich Verantwortung für die Sicherheit von Menschen, Fahrzeugtechnik und Umwelt. Seine Expertise erstreckt sich über Langstreckenreisen in Europa und den öffentlichen Nahverkehr.

## Persönliche Merkmale

Bernd ist familiär orientiert und ein Naturfreund, der seine Freizeit gerne mit Wanderungen in den Bergen verbringt. Er ist jedoch müde von den ständigen Medienberichten und hat ein Bewusstsein für den Klimawandel aufgebaut. Er unterstützt die Mobilitätswende, die Energiewende und ist ein großer Befürworter der Lebensmittelrettung.

## Herausforderungen

Trotz seiner Liebe zum Beruf erlebt Bernd stressige Arbeitstage, insbesondere wenn Ereignisse wie Klimaaktivisten-Demonstrationen den öffentlichen Verkehr beeinträchtigen.

## Spezifische Herausforderungen

Die "Hitzefrei"-Demonstration hat Bernd vor kurzem stark beeinträchtigt. Die Bundesstraße 465 und viele wichtige Haltestellen in Bad Wurzach waren betroffen, was zu erheblichen Störungen im Linienverkehr führte. Einige Fahrgäste äußerten ihre Unzufriedenheit, und die Frage nach einem erneuten Umstieg auf Autos wurde laut.

## Positive Entwicklungen

Bernd freut sich über die positive Entwicklung seit der Einführung des Deutschlandtickets. Es gibt wieder mehr Fahrgäste, die den Bus nutzen, und Bernd ist begeistert, dass er mehr Menschen befördern kann. Gleichzeitig erlebt er etwas weniger Verkehrsstau, da weniger PKWs unterwegs sind. Diese Entwicklung findet er äußerst positiv, da sie nicht nur die Umwelt entlastet, sondern auch seinen Arbeitsalltag erleichtert.
