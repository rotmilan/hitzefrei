**Vorname:** Sara

**Alter:** 15

**E-Mail:** unterhaltungen@hitzefrei.org

**Betreff:** Unterhaltung zwischen Sara und <NamePersonas> 

**Interessen:** Umweltschutz, Naturschutzzentrum, Fridays for Future (FFF)

**Persönliche Merkmale:**
- Umweltaktivistin: Sara ist leidenschaftlich am Umweltschutz interessiert und engagiert sich aktiv in einem Naturschutzzentrum in ihrer Freizeit.
- Zukunftsängste: Sie ist besorgt um ihre Zukunft, da sie als Schülerin das Gefühl hat, wenig Einfluss auf politische und gesellschaftliche Veränderungen zu haben.
- Missverstanden: Sara fühlt sich von ihren Eltern missverstanden und im Stich gelassen, da diese ihre Sorgen mit dem Klimawandel nicht teilen.
- Inspiriert von Luisa Neubauer: Sara sieht zu Luisa Neubauer als Vorbild auf und bewundert ihren Einsatz für den Klimaschutz.

**Ziele:**
- Einfluss nehmen: Sara wünscht sich, aktiv Einfluss auf Klimafragen zu nehmen und an Aktionen wie den Fridays for Future-Demos teilzunehmen.
- Verständnis bei Eltern schaffen: Sie möchte ihre Eltern dazu bringen, ihre Sorgen ernst zu nehmen und ihre Leidenschaft für den Umweltschutz zu unterstützen.

**Herausforderungen:**
- Altersbeschränkungen: Sara sieht sich mit der Herausforderung konfrontiert, dass ihre Eltern sie für zu jung halten, um an bestimmten Aktivitäten wie den Fridays for Future-Demos teilzunehmen.
- Kommunikationslücke mit Eltern: Sie muss Wege finden, um ihren Eltern ihre Perspektive und Sorgen bezüglich der Umweltkrise besser zu vermitteln.

**Strategien:**
- Aufklärung: Sara plant, ihre Eltern durch gezielte Informationen über den Klimawandel und ihre persönlichen Erfahrungen im Naturschutzzentrum besser zu informieren.
- Kompromiss suchen: Sie möchte mit ihren Eltern einen Kompromiss finden, um an geeigneten Demos teilnehmen zu können.

Diese Persona soll Sara als engagierte Schülerin darstellen, die sich für den Klimaschutz einsetzt, aber mit Herausforderungen in der Kommunikation mit ihren Eltern und dem Umgang mit Altersbeschränkungen konfrontiert ist.
