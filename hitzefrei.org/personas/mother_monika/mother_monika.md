Monika, eine umweltbewusste alleinerziehende Mutter im Alter von 42 Jahren, hat zwei Kinder und meistert ihre Rolle in einer herausfordernden Umgebung. Ihre Interessen reichen von der Förderung umweltfreundlicher Lebensweisen bis hin zu Volleyball. Monika arbeitet als Erzieherin und strebt danach, ihren Kindern ein gutes Vorbild zu sein.

## Beruflicher Hintergrund

Monika lebt einen umweltfreundlichen Lebensstil und bemüht sich, ein Vorbild für ihre beiden Kinder zu sein. Trotz der Herausforderungen als alleinerziehende Mutter steht sie vor der Aufgabe, das Bewusstsein für den Klimawandel unter Freunden und in ihrem Volleyballverein zu fördern. Trotz Widerstand und der Stigmatisierung als "Spaßverderberin" nimmt sie regelmäßig an Fridays For Future-Demonstrationen teil, um eine nachhaltige Zukunft zu unterstützen.

## Persönliche Merkmale

Monika ist familiär verbunden und schätzt ihre Rolle als Mutter. Ihre Naturverbundenheit und ihre Hingabe zum Umweltschutz sind stark ausgeprägt. Sie wurde einmal beim Retten von Lebensmitteln erwischt und vom Leiter eines Discounters darauf hingewiesen, dass dies eine Straftat ist. Glücklicherweise verzichtete der Leiter aus gutem Willen auf eine Anzeige.

## Herausforderungen

Monika hat oft das Gefühl, noch zu wenig für den Klimaschutz zu tun, obwohl sie schon seit Jahren nicht mehr mit dem Flugzeug fliegt und sich überwiegend vegan ernährt.

## Ziele

Monikas Ziele sind es, ein gutes Umweltvorbild zu sein und das Bewusstsein für den Klimawandel in sozialen Kreisen zu schärfen.

## Strategien

Monika setzt auf das Prinzip des Vorlebens und lebt weiterhin einen umweltfreundlichen Lebensstil, um andere zu inspirieren. Trotz Herausforderungen sucht sie diplomatische Wege, um das Bewusstsein für den Klimawandel in ihren sozialen Kreisen zu stärken.
